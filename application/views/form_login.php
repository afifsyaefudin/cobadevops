<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="utf-8" http-equiv="Content-Type" content="text/html;">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login Form</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php base_url();?>admin_assets/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php base_url();?>admin_assets/font-awesome/css/font-awesome.min.css">
	<style media="screen">
		body{
			/*background: #000023;*/
			background-image: url("admin_assets/img/1.jpg");
		}
		.container img{
			width: 130px;
			height: 150px;
			margin-top: 10px;
			margin-bottom: 30px;
		}
		.well{
			border-radius: 0px;
			margin-top: 10%;
			color: #616161;
		}
		.well hr{
			margin: 5px;
			border-color: #0077a3;
		}
		.header{
			font-size: 40px;
			color: #f7f7f7;
		}
		.header .fa{
			border: 2px solid #fcfcfc;
			border-radius: 50%;
			padding: 5px;
		}
		.container{
			padding-top: 5%;
		}
		.form-control{
			padding: 20px 10px;
			font-size: 20px;
		}
		.btn{
			padding: 5px 20px;
			font-size: 16px;
			border-radius: 0px
		}
	</style>
</head>
<body>
	<div class="container">
		<center>
			<span class="header"><img src="admin_assets/img/AYO PIKNIK.png"></i></span>
		</center>
		<div class="col-md-4 col-sm-12 col-md-offset-4">

			<?php 
			if($this->session->flashdata('alert')) {
				echo '<div class="alert alert-warning alert-message">';
				echo $this->session->flashdata('alert');
				echo '</div>';
			}
			 ?>
			<form class="well" action="" method="post">
				<h3><i class="fa fa-user"></i> Please Sign In</h3>
				<hr/>
				<br/>

				<div class="form-group">
					<label>Username</label>
					<input type="text" name="username" class="form-control" placeholder="Username">
				</div>

				<div class="form-group">
					<label>Password</label>
					<input type="password" name="password" class="form-control" placeholder="Password">
				</div>

				<div class="form-group">
					<button type="submit" class="btn btn-warning" name="submit" value="Submit">Sign In..</button>
					<button type="reset" class="btn btn-default">Cancel</button>
				</div>
			</form>
		</div>
	</div>
	<!-- JQuery -->
	<script src="<?php base_url();?>admin_assets/js/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?php base_url();?>admin_assets/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$('.alert-message').alert().delay(2000).slideUp('slow');
	</script>
</body>
</html>