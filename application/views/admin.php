::<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Administrator </title>

    <!-- Bootstrap -->
    <link href="http://localhost/olshop/admin_assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="http://localhost/olshop/admin_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="http://localhost/olshop/admin_assets/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><i class="fa fa-plane"></i> <span>Ayo Piknik</span></a>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-plus"></i> Tambah</a>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Forms</a>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> UI Elements</span></a>
                  </li>
                  <li><a><i class="fa fa-table"></i> Tables</span></a>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation</span>
                  </li>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-user"> Administrator</i>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Profile</a></li>
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="<?php site_url();?>Login" ><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->


        <!-- page content -->
      <div class="right_col" role="main">
        <div class="x_panel">
      <div class="x_title">
        <h2>Tambah Wisata</h2>
      <div class="clearfix"></div>
      
         
  </div>

  <div class="x_content">
    <br />

    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="" enctype="multipart/form-data" method="post">
      <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Nama Wisata</label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="text" name="nama" class="form-control col-md-7 col-xs-12" value="">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Tiket</label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="text" name="nama" class="form-control col-md-7 col-xs-12" value="">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Lokasi</label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="text" name="nama" class="form-control col-md-7 col-xs-12" value="">
        </div>
      </div>


      <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Gambar</label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="file" name="" class="form-control col-md-7 col-xs-12" name="gambar">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-2 col-sm-2 col-xs-12">Deskripsi</label>
        <div class="col-md-9 col-sm-6">
          <textarea class="form-control" rows="4" name="desk" value=""></textarea>
        </div>
      </div>

      <div class="ln_solid"></div>

      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <button type="submit" class="btn btn-success" name="submit" value="Submit">Submit</button>
          <button type="reset" class="btn btn-warning">Cancel</button>
        </div>
      </div>

    </form>
  </div>
</div>
        </div>

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
           <a href="https://ayopiknik.com">Ayo Piknik</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="http://localhost/olshop/admin_assets/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="http://localhost/olshop/admin_assets/js/bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="http://localhost/olshop/admin_assets/js/custom.min.js"></script>
    <script type="text/javascript">
      $('.alert-message').alert().delay(3000).slideUp(slow);
    </script>
  </body>
</html>